const express = require('express');
const app = express();

const bodyParser = require('body-parser');
const cors = require('cors');
const mongoose = require('mongoose');
const mongodb = require('mongodb');

const todoRoutes = express.Router();

const port = 4000;

let Todo = require('./todo.model');

const connection = mongoose.connection;

mongoose.connect('mongodb://127.0.0.1:27017/todos', {useNewUrlParser: true, useUnifiedTopology: true});

connection.once('open', function(){
  console.log("MongoDB connection established successfully.");
});

app.use(cors());
app.use(bodyParser.json());

todoRoutes.route('/').get(function(req, res){
    Todo.find(function(err, todos) {
        if (err){
            console.log(err);
        } else {
            res.json(todos);
        }
    })
});

todoRoutes.route('/:id').get(function(req, res){
    Todo.findById(req.params.id, function(err, todo){
        res.json(todo);
    });
});

todoRoutes.route('/add').post(function(req,res){
    let todo = new Todo(req.body);

    todo.save().then(todo => {
        res.status(200).json({'todo': 'Activity added successfully.'});
    }).catch(err => {
        res.status(400).send('Adding new activity failed!');
    });
});

todoRoutes.route('/update/:id').post(function(req,res){
    let id = req.params.id;
    Todo.findById(id, function(err, todo){
        if (!todo){
          res.status(404).send("Data not found.");
        } else {
          todo.todo_description = req.body.todo_description;
          todo.todo_date = req.body.todo_date;
          todo.todo_priority = req.body.todo_priority;
          todo.todo_completed = req.body.todo_completed;

          todo.save().then(todo => {
              res.json('Activity updated successfully.');
          }).catch(err => {
              res.status(400).send('Adding new activity failed!');
          });
        }
    });
});


todoRoutes.route('/:id').delete(function(req,res){
    Todo.deleteOne({_id: mongodb.ObjectID(req.params.id) }, function(err){
        if (err){
          res.status(404).send(err);
        } else {
          res.json('Activity deleted successfully.');
        }
    });
});

app.use('/todos',todoRoutes);

app.listen(port, function(){
    console.log("Server is running on port "+port);
});
