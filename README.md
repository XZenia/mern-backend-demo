## MERN Stack Demo - To Do List

This is a simple backend of a to do list app. Again, pretty basic stuff. 
 
### Prerequisites

You must have the following to run this project locally.
	1. [MongoDB](https://docs.mongodb.com/manual/administration/install-community/)
	2. [Node](https://nodejs.org/en/download/)
	3. npm (Node Package Manager)
	4. Express 
	5. bodyParser, nodemon, cors and mongoose (All of these can be installed in the project through npm)
 
### Deployment

Run MongoDB Daemon first by entering:

### 'mongod'

<br/>

Then in the project directory, run the command:

### `nodemon server`

Runs the app's backend.<br />
Open [http://localhost:4000](http://localhost:4000) to view it in the browser. You can change the port number through the port variable in server.js.


<br/>
Developed, ran and tested using Ubuntu 18.04 so your process may vary depending on your OS.